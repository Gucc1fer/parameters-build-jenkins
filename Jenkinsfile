def gv
pipeline {
    agent any
    parameters {
        choice(name:'VERSION', choices: ['1.1.0', '1.2.0', '1.3.0'], description: 'Build Version')
        string(name: 'DEPLOY_ENV', defaultValue: 'production', description: '')
        booleanParam(name: 'EXECUTE_TESTS', defaultValue: true, description: '')
    }
    stages {
        stage('init'){
            input {
                message "Should we continue?"
                ok "Yes, we should"
                submitter "alice, bob"
                parameters {
                    string(name: 'PERSON', defaultValue: 'Guest', description: 'Who should I say hello to?')
                }
            }
            steps{
                echo "Hello, ${PERSON}, welcome to jenkins!"
                script{
                    gv = load "script.groovy"
                }
            }
        }
        stage ('build'){
            steps {
                script{
                    emailext body: "BUILD STARTED: Job ${env.JOB_NAME} [${env.BUILD_NUMBER}]", 
                        recipientProviders: [[$class: 'DevelopersRecipientProvider'], 
                                            [$class: 'RequesterRecipientProvider']], 
                        subject: "STARTED - Job ${env.JOB_NAME}",
                        replyTo: 'do-not-reply@persistent.com'
                        
                    gv.buildApp()
                }
            }
        }
        stage('test'){
            when {
                expression {
                    params.EXECUTE_TESTS
                }
            }
            steps {
                script{
                    gv.testApp()
                }
            }
        }
        stage('deploy'){
            steps {
                script{
                    gv.deployApp()
                }
            }
        }
    }
    post {
        success {
            emailext body: "BUILD SUCCESS: Job ${env.JOB_NAME} [${env.BUILD_NUMBER}]", 
                recipientProviders: [[$class: 'DevelopersRecipientProvider'], 
                                    [$class: 'RequesterRecipientProvider']], 
                subject: "SUCCESS - Job ${env.JOB_NAME}",
                replyTo: 'do-not-reply@persistent.com'
        }
        failure {
            emailext body: "BUILD FAILED: Job ${env.JOB_NAME} [${env.BUILD_NUMBER}]", 
                recipientProviders: [[$class: 'DevelopersRecipientProvider'], 
                                    [$class: 'RequesterRecipientProvider']], 
                subject: "FAILED - Job ${env.JOB_NAME}",
                replyTo: 'do-not-reply@persistent.com'
        }
    }
}