def buildApp(){
    echo 'Building the application...'
}

def testApp(){
    echo 'Testing the application...'
}

def deployApp(){
    echo 'Deploying the application...'
    echo "Deploy ENV set to ${params.DEPLOY_ENV}"
    echo "Deploying version ${params.VERSION}"
}
return this